﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings_Search_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string temp = "The world is filled with engineering graduates. However, what we need are developers.";

            //checking out index of
            //the moment it finds world (the very first time), it gives the position where it was found
            Console.WriteLine("I found world at " + temp.IndexOf("world"));

            //checking out last index of
            //this will find the last time i occurs in the sentence
            Console.WriteLine("I found i at " + temp.LastIndexOf("i"));

            //checking out starts with
            //this will check if a particular string starts with a specific string
            if(temp.StartsWith("The"))
            {
                Console.WriteLine("I found something starts with 'neer'");
            }
            else
            {
                Console.WriteLine("I did not found something starts with 'neer'");
            }

            //checking out ends with
            //this will check if a particular string ends with a specific string
            if (temp.EndsWith("developers."))
            {
                Console.WriteLine("I found something ends with 'developers.'");
            }
            else
            {
                Console.WriteLine("I did not found something ends with 'developers.'");
            }

            //now lets check if a particular sub string is in a string
            if(temp.Contains("engineering"))
            {
                Console.WriteLine("I found something ");
            }
            else
            {
                Console.WriteLine("I did not found something ");
            }

            //lets pick a substring out of a string by giving its location, and the length we want from it
            //the first parameter indicates the location from where I want to pick stuff
            //the second parameter indicates the length of string I wish to pick
            Console.WriteLine(" I found this from the main string - " + temp.Substring(10, 15));

            //this is to stop the console from vanishing
            Console.ReadLine();
        }
    }
}
